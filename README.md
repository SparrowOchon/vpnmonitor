# VPNMonitor

A real time monitoring service of high valued processes ensuring consistent secure OpenVPN tunneling and no leakage.

### Features:

- Low execution time overhead
- Dynamic Error Recovery and Fault-Tolerance
- Realtime VPN Checking (No Timers)
- Notification of High Value Process errors/failures (Email/Sms)
- Notification System on Failure (Email/Sms)
- Easily modifiable
- Automatic Disabling of High Value Proccess on Kill attempt

### Using

**ALL HIGH VALUE PROCESSES MUST BE TURNED OFF AND MUST NOT HAVE AN ACTIVE OPENVPN SESSION**

- Run the vpnmonitor `python3 vpnmon -v <path/to/openvpn.opvn> -p <path/to/program | process.service>`
- Make sure the Machine is indeed secured by checking the public ip with curl `curl -L https://canihazip.com/s`

##### NOTE: Email and openvpn path can be set at the top of vpnmon.
