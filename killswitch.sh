#!/bin/bash
#================================================================================================
#
# FILENAME :        killswitch.sh
#
# DESCRIPTION : Activate and Flush iptable rules depending on parameters passed.
#				if no parameters passed flush the iptables. 
#				Otherwise. Activate iptables to block all non openvpn traffic.
#
# USAGE:
#		killswitch.sh <CONNECTION_TYPE>:<VPN_IP>:<VPN_PORT>:<VPN_TUNNEL>
#
#
# AUTHOR :   Network Silence        START DATE :    30 Dec 2019
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2019, Network Silence
#		All rights reserved.
#
#
#
#
#===============================================================================================
main(){
	if [[ $(id -u) -ne 0 ]]; then
		print_error "Run this as sudo."
	elif [[ $# -eq 0 ]]; then
		deactivate
	else
		validate_parameters "$@"
	fi
}
deactivate(){
	iptables -P INPUT ACCEPT
	iptables -P FORWARD ACCEPT
	iptables -P OUTPUT ACCEPT
	iptables -t nat -F
	iptables -t mangle -F
	iptables -F
	iptables -X
}
activate(){
	vpn_protocol="$1"
	vpn_ip="$2"
	vpn_port="$3"
	vpn_tunnel="$4"

	echo "$@"
	iptables --flush
	iptables --delete-chain
	iptables -t nat --flush
	iptables -t nat --delete-chain
	iptables -P OUTPUT DROP
	iptables -A INPUT -j ACCEPT -i lo
	iptables -A OUTPUT -j ACCEPT -o lo
	iptables -A INPUT --src 192.168.0.0/24 -j ACCEPT -i enp1s0
	iptables -A OUTPUT -d 192.168.0.0/24 -j ACCEPT -o enp1s0
	iptables -A OUTPUT -j ACCEPT -d "$vpn_ip" -o enp1s0 -p "$vpn_protocol" -m "$vpn_protocol" --dport "$vpn_port"
	iptables -A INPUT -j ACCEPT -s "$vpn_ip" -i enp1s0 -p "$vpn_protocol" -m "$vpn_protocol" --sport "$vpn_port"
	iptables -A INPUT -j ACCEPT -i "$vpn_tunnel"
	iptables -A OUTPUT -j ACCEPT -o "$vpn_tunnel"
}
print_error(){
	echo "USAGE: $0 <CONNECTION_TYPE>:<VPN_IP>:<VPN_PORT>:<VPN_TUNNEL> [<CONNECTION_TYPE>:<VPN_IP>:<VPN_PORT>:<VPN_TUNNEL>]"
	echo "$1"
	exit 1
}
validate_parameters(){
	for arg in "$@"
	do
		set -- "$arg"
		IFS=":"
		declare -a Array=($*)
		validate_protocol "${Array[0]}"
		validate_ip "${Array[1]}"
		validate_port "${Array[2]}"
		activate "${Array[0]}" "${Array[1]}" "${Array[2]}"
	done

}
validate_ip(){
	ip_address="$1"

	if ! [[ "$ip_address" =~ ^(([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.){3}([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$ ]]; then
		print_error "IP not valid"
	fi

}
validate_port(){
	port="$1"
	if ! [[ "$port" =~ ^()([1-9]|[1-5]?[0-9]{2,4}|6[1-4][0-9]{3}|65[1-4][0-9]{2}|655[1-2][0-9]|6553[1-5])$ ]];then
		print_error "Port not valid"
	fi

}
validate_protocol(){
	protocol="$1"
	protocol=$(echo "$protocol" | tr '[:upper:]' '[:lower:]')
	if ! [[ $protocol =~ ["udp"|"tcp"] ]];then
		print_error "Only supports UDP and TCP"
	fi
}
main "$@"
